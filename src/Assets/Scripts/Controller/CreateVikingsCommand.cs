﻿using System.Linq;
using Assets.Scripts.Known.Powerup;
using Assets.Scripts.Utils;
using Game.Views;
using Game.Vikings;
using strange.extensions.command.impl;

namespace Game.Map
{
    public class CreateVikingsCommand : Command
    {
        [Inject]
        public IVikingGod VikingGod { get; set; }

        [Inject]
        public ILevel Level { get; set; }

        [Inject]
        public GameStatus GameStatus { get; set; }

        [Inject]
        public GameParameters GameParameters { get; set; }


        public override void Execute()
        {
            VikingGod.Init();

            var vikingCount = Level.Map.Count(v => v.GenerationPointType == MapGenerationPointType.Viking) - GameParameters.VikingCountDelta;
            if (GameStatus.ActivePowerUp == PowerUp.EasyMode)
            {
                vikingCount -= GameParameters.VikingCountDelta;
            }
            if (GameStatus.ActiveNegativePowerUp == NegativePowerUp.HardMode)
            {
                vikingCount += GameParameters.VikingCountDelta;
            }

            var vikingPositions = Level.Map.FindAll(v => v.GenerationPointType == MapGenerationPointType.Viking).Shuffle().Take(vikingCount);

            foreach (var viking in vikingPositions.Select(vikingPosition => VikingCreator
                .CreateViking()
                .WithPosition(vikingPosition.Position)
                .WithPointValue(GameParameters.PointsPerKill)))
            {
                VikingGod.Vikings.Add(viking);
            }
        }
    }
}