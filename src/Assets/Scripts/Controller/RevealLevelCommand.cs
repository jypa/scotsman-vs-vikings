﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Known.Powerup;
using Game;
using Game.Map;
using Game.Signals;
using strange.extensions.command.impl;

namespace Assets.Scripts.Controller
{
    public class RevealLevelCommand : Command
    {
        [Inject]
        public Scotsman Scotsman { get; set; }

        [Inject]
        public ILevel Level { get; set; }

        [Inject]
        public UpdateFogOfWarSignal UpdateFogOfWarSignal { get; set; }

        [Inject]
        public GameStatus Status { get; set; }

        public override void Execute()
        {
            if (Status.ActiveNegativePowerUp != NegativePowerUp.FogOfWar &&
                Status.ActiveNegativePowerUp != NegativePowerUp.PitchBlack) return;

            BlackOutMap();

            var neighboringRoads = Level.GetNeighboringRoads(Scotsman.Position);
            var directions = GetDirections(Scotsman.Position, neighboringRoads);

            foreach (var direction in directions)
            {
                RevealDirection(Scotsman.Position, direction);
            }

            UpdateFogOfWarSignal.Dispatch();
        }

        private void BlackOutMap()
        {
            foreach (var mapPoint in Level.Map)
            {
                mapPoint.Visible = false;

                if (Status.ActiveNegativePowerUp == NegativePowerUp.PitchBlack)
                {
                    mapPoint.Visited = false;
                }
            }
        }

        private void RevealDirection(Position position, GameAction direction)
        {
            while (true)
            {
                RevealPoint(position);
                RevealAroundCorners(position);
                RevealGrass(position);

                var newPosition = Movement.GetNewPosition(position, direction);

                if (Level.CanWalk(newPosition))
                {
                    position = newPosition;
                }
                else
                {
                    break;
                }
            } 

            RevealAroundCorners(position);
        }

        private void RevealGrass(Position position)
        {
            var grass = Level.GetNeighboringGrass(position);
            foreach (var point in grass)
            {
                RevealPoint(point);
            }
        }

        private void RevealPoint(Position position)
        {
            var point = Level.GetPoint(position);
            point.Visited = true;
            point.Visible = true;
        }

        private void RevealAroundCorners(Position position)
        {
            foreach (var point in Level.GetNeighboringRoads(position))
            {
                RevealPoint(point);
            }
        }

        private static IEnumerable<GameAction> GetDirections(Position position, IEnumerable<Position> neighboringRoads)
        {
            return neighboringRoads.Select(neighboringRoad => Movement.GetDirection(position, neighboringRoad))
                .Where(dir => dir.HasValue)
                .Select(dir => dir.Value);
        }
    }
}