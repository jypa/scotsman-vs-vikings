﻿using System.Linq;
using Assets.Scripts.Signals;
using Assets.Scripts.Utils;
using Game;
using Game.Map;
using Game.Views;
using Game.Vikings;
using strange.extensions.command.impl;

namespace Assets.Scripts.Controller
{
    public class RespawnVikingsCommand : Command
    {
        [Inject]
        public ILevel Level { get; set; }

        [Inject]
        public IVikingGod VikingGod { get; set; }

        [Inject]
        public Scotsman Scotsman { get; set; }

        [Inject]
        public RespawnVikingSingal RespawnVikingSignal { get; set; }

        [Inject]
        public GameParameters GameParameters { get; set; }

        public override void Execute()
        {
            var removeTimers = VikingGod.Respawns
                // Timer is up
                .Where(timer => timer.RoundCount-- <= 0)
                // Try to respawn
                .Where(timer => RespawnViking())
                .ToList();

            foreach (var timer in removeTimers)
            {
                VikingGod.Respawns.Remove(timer);
            }

        }

        private bool RespawnViking()
        {
            var vikingPositions = Level.Map.FindAll(v => v.GenerationPointType == MapGenerationPointType.Viking).Shuffle();

            foreach (var viking in vikingPositions
                .Where(vikingPosition => vikingPosition.Position != Scotsman.Position
                && NoVikingAt(vikingPosition.Position)))
            {
                var vikingDto = VikingCreator
                    .CreateViking()
                    .WithPosition(viking.Position)
                    .WithPointValue(GameParameters.PointsPerKill);

                VikingGod.Vikings.Add(vikingDto);
                RespawnVikingSignal.Dispatch(vikingDto);

                return true;
            }

            return false;
        }

        private bool NoVikingAt(Position position)
        {
            return VikingGod.Vikings.All(vikingDto => vikingDto.Position != position);
        }
    }
}