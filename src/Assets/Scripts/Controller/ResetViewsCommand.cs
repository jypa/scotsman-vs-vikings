﻿using Game.Signals;
using strange.extensions.command.impl;

namespace Game.Map
{
    public class ResetViewsCommand : Command
    {
        [Inject]
        public ResetViewsSignal ResetViewsSignal { get; set; }

        public override void Execute()
        {
            ResetViewsSignal.Dispatch();
        }
    }
}