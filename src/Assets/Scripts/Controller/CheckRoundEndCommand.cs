﻿using System.Collections.Generic;
using Game;
using Game.Map;
using Game.Signals;
using strange.extensions.command.impl;

namespace Assets.Scripts.Controller
{
    public class CheckRoundEndCommand : Command
    {
        [Inject]
        public Scotsman Scotsman { get; set; }

        [Inject]
        public ILevel Level { get; set; }

        [Inject]
        public GameStatus GameStatus { get; set; }

        [Inject]
        public GameParameters GameParameters { get; set; }

        [Inject]
        public RoundEndSignal RoundEndSignal { get; set; }

        public override void Execute()
        {
            if (Scotsman.Coins != Level.CoinCount) return;
            GameStatus.RoundActive = false;
            GameStatus.Victory = true;
            GameStatus.RoundBonuses.Add(new KeyValuePair<Bonus, int>(Bonus.Victory,GameParameters.LevelFinishBonus));
            RoundEndSignal.Dispatch();
            Fail();
        }
    }
}