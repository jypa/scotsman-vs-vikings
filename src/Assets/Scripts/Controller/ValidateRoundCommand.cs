﻿using strange.extensions.command.impl;

namespace Game
{
    public class ValidateRoundCommand : Command
    {
        [Inject]
        public GameStatus GameStatus{ get; set; }

        public override void Execute()
        {
            if (!GameStatus.RoundActive)
                Fail();
        }
    }
}