﻿using Assets.Scripts.Known.Powerup;
using Game;
using Game.Views.Mediators;
using strange.extensions.command.impl;

namespace Assets.Scripts.Controller
{
    public class StartPowerUps : Command
    {
        [Inject]
        public GameStatus Status { get; set; }

        [Inject]
        public TimerControlSignal TimerControlSignal { get; set; }

        public override void Execute()
        {
            if (Status.ActiveNegativePowerUp == NegativePowerUp.RealTime)
            {
                TimerControlSignal.Dispatch(TimerControl.Start);
            }
        }
    }
}