﻿using System.Linq;
using Assets.Scripts.Known.Powerup;
using Assets.Scripts.Signals;
using Assets.Scripts.Utils;
using Game;
using Game.Map;
using Game.Signals;
using strange.extensions.command.impl;

namespace Assets.Scripts.Controller
{
    public class AttackCommand : Command
    {
        [Inject]
        public Position Position { get; set; }

        [Inject]
        public IVikingGod VikingGod { get; set; }

        [Inject]
        public Scotsman Scotsman { get; set; }

        [Inject]
        public KillVikingSignal KillVikingSignal { get; set; }

        [Inject]
        public ILevel Level { get; set; }

        [Inject]
        public GameStatus GameStatus { get; set; }

        [Inject]
        public GameParameters GameParameters { get; set; }

        [Inject]
        public GuiTextSignal GuiTextSignal { get; set; }

        public override void Execute()
        {
            if (!Scotsman.Weapons.Any()) return;

            var weapon = Scotsman.Weapons[0];

            var vikings = VikingGod.Vikings
                .Where(x => Level.GetNeighboringRoads(Position, weapon.Reach).Contains(x.Position))
                .Shuffle().ToList();

            var killCount = GetKillCount();

            HandleStunningWeapon();

            foreach (var vikingDto in vikings)
            {
                // Kill or stun
                if (killCount-- > 0)
                {
                    VikingGod.Vikings.Remove(vikingDto);
                    VikingGod.Respawns.Add(GetRespawnTimer());
                    KillVikingSignal.Dispatch(vikingDto);

                    var pointsFromKill = HandleCombo(vikingDto);

                    GuiTextSignal.Dispatch(new GuiText(GuiTextType.Points, vikingDto.Position, string.Format("{0}", pointsFromKill)));
                }
                else
                {
                    vikingDto.Stunned = GameParameters.VikingStunPeriod;
                }
            }

            Scotsman.Weapons.Remove(weapon);
        }

        private int HandleCombo(VikingDto vikingDto)
        {
            var multiplier = GameParameters.ComboMultiplierAlgorithm(Scotsman.ComboMultiplier++);
            var killPoints = multiplier*vikingDto.Value;
            GameStatus.RoundPoints += killPoints;
            if (multiplier > 1)
            {
                GuiTextSignal.Dispatch(new GuiText(GuiTextType.Combo, Scotsman.Position, string.Format("{0}x", multiplier)));
            }
            return killPoints;
        }

        private int GetKillCount()
        {
            if (GameStatus.ActiveNegativePowerUp == NegativePowerUp.DontKillAllVikings)
            {
                return GameParameters.KillVikingsCount;
            }

            if (GameStatus.ActiveNegativePowerUp == NegativePowerUp.BluntWeapon)
            {
                return 0;
            }

            return 8;
        }

        private void HandleStunningWeapon()
        {
            if (GameStatus.ActivePowerUp != PowerUp.StunningWeapon) return;
            foreach (var viking in VikingGod.Vikings)
            {
                viking.Stunned = GameParameters.AllStunPeriod;
            }
            foreach (var respawn in VikingGod.Respawns)
            {
                respawn.RoundCount += GameParameters.DelayRespawnPeriod;
            }
        }

        private RespawnTimer GetRespawnTimer()
        {
            var respawnTime = GameParameters.NormalRespawn;
            if (GameStatus.ActivePowerUp == PowerUp.EasyMode)
            {
                respawnTime = GameParameters.SlowRespawn;
            }
            if (GameStatus.ActiveNegativePowerUp == NegativePowerUp.HardMode)
            {
                respawnTime = GameParameters.FastRespawn;
            }

            return new RespawnTimer(respawnTime);
        }
    }
}