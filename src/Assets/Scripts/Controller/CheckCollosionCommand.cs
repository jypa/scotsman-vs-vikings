﻿using System.Linq;
using Game;
using Game.Signals;
using strange.extensions.command.impl;

namespace Assets.Scripts.Controller
{
    public class CheckCollosionCommand : Command
    {
        [Inject]
        public IVikingGod VikingGod { get; set; }

        [Inject]
        public Scotsman Scotsman { get; set; }

        [Inject]
        public GameStatus GameStatus { get; set; }

        [Inject]
        public RoundEndSignal RoundEndSignal { get; set; }

        [Inject]
        public AttackSignal AttackSignal { get; set; }

        public override void Execute()
        {
            // Check collosion
            if (VikingGod.Vikings.All(viking => viking.Position != Scotsman.Position)) return;

            GameStatus.RoundActive = false;
            RoundEndSignal.Dispatch();
            Fail();
        }
    }
}