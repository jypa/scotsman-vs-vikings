﻿using Game;
using Game.Signals;
using strange.extensions.command.impl;

namespace Assets.Scripts.Controller
{
    public class GameFlowCommand : Command
    {
        [Inject]
        public GameStatus GameStatus { get; set; }

        [Inject]
        public GameFlowAction GameFlowAction { get; set; }

        [Inject]
        public RoundStartSignal RoundStartSignal { get; set; }

        public override void Execute()
        {
            switch (GameFlowAction)
            {
                case GameFlowAction.Start:
                    StartGame();
                    break;

            }
        }

        private void StartGame()
        {
            GameStatus.StartNewRound();
            RoundStartSignal.Dispatch();
        }
    }
}