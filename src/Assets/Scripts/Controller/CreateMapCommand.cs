﻿using System.Collections.Generic;
using System.Linq;
using strange.extensions.command.impl;

namespace Game.Map
{
    public class CreateMapCommand : Command
    {
        [Inject]
        public ILevel Level { get; set; }

        [Inject]
        public IPointCreator PointCreator { get; set; }

        [Inject]
        public GameStatus GameStatus { get; set; }

        public override void Execute()
        {
            var mapString = GameStatus.Map != null ? MapStorage.Maps[GameStatus.Map] : MapStorage.Maps.First().Value;

            mapString = AddGrassBuffer(mapString, 10);

            Level.Map = new List<MapPoint>();

            Level.CoinCount = 0;

            for (var y = 0; y < mapString.Length; y++)
            {
                var row = mapString[y].ToCharArray();

                for (var x = 0; x < row.Length; x++)
                {
                    // Rotate Level upside down
                    var position = new Position(x, mapString.Length - y - 1);
                    var point = PointCreator.CreatePoint(position, row[x]);
                    Level.Map.Add(point);

                    if (point.Item is Coin)
                    {
                        Level.CoinCount++;
                    }
                }
            }
        }

        private string[] AddGrassBuffer(string[] mapString, int bufferSize)
        {
            var newArray = new string[mapString.Length + 2 * bufferSize];
            var levelWidth = mapString[0].Length;

            for (int i = 0; i < bufferSize; i++)
                newArray[i] = new string('G', levelWidth + 2 * bufferSize);

            for (int i = 0; i < mapString.Length; i++)
                newArray[i + bufferSize] = new string('G', bufferSize) + mapString[i] + new string('G', bufferSize);

            for (int i = 0; i < bufferSize; i++)
                newArray[i + mapString.Length + bufferSize] = new string('G', levelWidth + 2 * bufferSize);

            return newArray;
        }
    }
}