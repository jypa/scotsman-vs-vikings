﻿using System.Linq;
using Assets.Scripts.Known.Powerup;
using strange.extensions.command.impl;

namespace Game.Map
{
    public class CreateScotsmanCommand : Command
    {
        [Inject]
        public ILevel Level { get; set; }

        [Inject]
        public Scotsman Scotsman { get; set; }

        [Inject]
        public GameStatus GameStatus { get; set; }

        public override void Execute()
        {
            var startPoint = Level.Map.FirstOrDefault(p => p.GenerationPointType == MapGenerationPointType.Scotsman);

            var startPosition = startPoint == null ? new Position(0, 0) : startPoint.Position;
            Scotsman.Position = startPosition;

            Scotsman.Coins = 0;
            Scotsman.Weapons.Clear();

            Scotsman.MaxNumberOfWeapons = 3;

            if (GameStatus.ActivePowerUp == PowerUp.MoreWeapons)
            {
                Scotsman.MaxNumberOfWeapons = 4;
            }
            if (GameStatus.ActiveNegativePowerUp == NegativePowerUp.FewerWeapons)
            {
                Scotsman.MaxNumberOfWeapons = 2;
            }
        }
    }
}