﻿using System;
using System.Linq;
using Assets.Scripts.Utils;
using Game;
using Game.Map;
using Game.Signals;
using strange.extensions.command.impl;

namespace Assets.Scripts.Controller
{
    public class VikingsActionCommand : Command
    {
        [Inject]
        public IVikingGod VikingGod { get; set; }

        [Inject]
        public Scotsman Scotsman { get; set; }

        [Inject]
        public ILevel Level { get; set; }

        [Inject]
        public UpdateVikingSignal UpdateVikingSignal { get; set; }

        [Inject]
        public AttackSignal AttackSignal { get; set; }

        private readonly Random _random = new Random();

        public override void Execute()
        {
            // Limit pathfinding distance for performance reasons
            var pathFinder = new PathFinder(Level, Scotsman.Position, VikingGod.Vikings, 6);

            foreach (var viking in VikingGod.Vikings)
            {
                if (viking.Position == Scotsman.Position) continue;

                if (viking.Stunned-- > 0) continue;

                Position newPosition;

                if (_random.NextDouble() < 0.85)
                {
                    // Check if pathfinder found path or move randomly around
                    newPosition = pathFinder.GetNextMove(viking.Position) ?? GetRandomMove(viking.Position);
                }
                else
                {
                    newPosition = GetRandomMove(viking.Position);
                }


                // If there is other viking at this position
                if (IsAnyVikingAt(newPosition))
                {
                    newPosition = GetRandomMove(viking.Position);

                    if (IsAnyVikingAt(newPosition)) continue;
                }

                if (newPosition == Scotsman.Position && Scotsman.Weapons.Any())
                {
                    AttackSignal.Dispatch(newPosition);
                }
                else
                {
                    viking.Position = newPosition;
                    UpdateVikingSignal.Dispatch(viking);
                }
                
            }
        }

        private bool IsAnyVikingAt(Position newPosition)
        {
            return VikingGod.Vikings.Any(x => x.Position == newPosition);
        }

        private Position GetRandomMove(Position position)
        {
            var directions = Level.GetNeighboringRoads(position);
            return directions[_random.Next(0, directions.Count)];
        }

    }
}