﻿using System.Linq;
using Game;
using Game.Map;
using Game.Signals;
using Game.Views.Mediators;
using strange.extensions.command.impl;

namespace Assets.Scripts.Controller
{
    public class ScotsmanActionCommand : Command
    {
        [Inject]
        public Scotsman Scotsman { get; set; }

        [Inject]
        public GameAction GameAction { get; set; }

        [Inject]
        public ScotsmanMoveSignal ScotsmanMoveSignal { get; set; }

        [Inject]
        public AttackSignal AttackSignal { get; set; }

        [Inject]
        public ILevel Level { get; set; }

        [Inject]
        public IVikingGod VikingGod { get; set; }

        [Inject]
        public TimerControlSignal TimerControlSignal { get; set; }

        public override void Execute()
        {
            // Move
            if (GameAction == GameAction.MoveDown
                || GameAction == GameAction.MoveLeft
                || GameAction == GameAction.MoveRight
                || GameAction == GameAction.MoveUp)
            {
                var newPosition = Movement.GetNewPosition(Scotsman.Position, GameAction);
                if (!Level.CanWalk(newPosition))
                {
                    Fail();
                    return;
                }

                MoveOrAttack(newPosition);
                TimerControlSignal.Dispatch(TimerControl.Reset);
            }

            if (GameAction == GameAction.Pause)
            {
                ClearComboMultiplier();
            }
        }

        private void MoveOrAttack(Position newPosition)
        {
            if (VikingGod.Vikings.Any(x => x.Position == newPosition))
            {
                AttackSignal.Dispatch(Scotsman.Position);
            }
            else
            {
                Scotsman.Position = newPosition;
                ScotsmanMoveSignal.Dispatch(newPosition);
                ClearComboMultiplier();
            }
        }

        private void ClearComboMultiplier()
        {
            Scotsman.ComboMultiplier = 0;
        }
    }
}