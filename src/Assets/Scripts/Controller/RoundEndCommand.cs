﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Signals;
using Game;
using Game.Map;
using Game.Views.Mediators;
using strange.extensions.command.impl;

namespace Assets.Scripts.Controller
{
    public class RoundEndCommand : Command
    {
        [Inject]
        public ShowRoundEndGuiSignal ShowRoundEndGuiSignal { get; set; }

        [Inject]
        public GameStatus GameStatus { get; set; }

        [Inject]
        public GameParameters GameParameters { get; set; }

        [Inject]
        public TimerControlSignal TimerControlSignal { get; set; }

        public override void Execute()
        {
            TimerControlSignal.Dispatch(TimerControl.Stop);

            if (GameStatus.ActivePowerUp != null)
            {
                GameStatus.RoundBonuses.Add(
                    new KeyValuePair<Bonus, int>(
                        Bonus.PowerUpPenalty,
                        Convert.ToInt32(Math.Floor(GameStatus.RoundPoints * GameParameters.PowerUpPenaltyPercent))
                    ));
            }
            if (GameStatus.ActiveNegativePowerUp != null)
            {
                GameStatus.RoundBonuses.Add(
                    new KeyValuePair<Bonus, int>(
                        Bonus.NegativePowerUpBonus,
                        Convert.ToInt32(Math.Floor(GameStatus.RoundPoints * GameParameters.NegativePowerUpBonusPercent))
                    ));
            }

            GameStatus.RoundTotal = GameStatus.RoundPoints + GameStatus.RoundBonuses.Sum(x => x.Value);
            if (GameStatus.BestRound < GameStatus.RoundTotal)
            {
                GameStatus.BestRound = GameStatus.RoundTotal;
            }

            GameStatus.Experience += GameStatus.RoundTotal;

            // Level up
            GameStatus.LevelUp = false;
            GameStatus.PowerUpUnlock = false;
            GameStatus.MapUnlocked = false;

            while (GameStatus.Experience > GameStatus.NextLevel)
            {
                GameStatus.Level++;
                GameStatus.LastLevel = GameStatus.NextLevel;
                GameStatus.NextLevel += (GameStatus.Level * GameParameters.LevelCap);
                GameStatus.LevelUp = true;


                if (GameStatus.Level % GameParameters.PowerUpUnlockLevel == 0)
                {
                    GameStatus.PowerUpUnlock = true;
                }

                if (GameStatus.Level % GameParameters.MapUnlockLevel == 0)
                {
                    GameStatus.MapUnlocked = true;

                    var nextMap = MapStorage.Maps.FirstOrDefault(x => !GameStatus.UnlockedMaps.Contains(x.Key)).Key;

                    if (nextMap != null)
                        GameStatus.UnlockedMaps.Add(nextMap);
                }
            }

            ShowRoundEndGuiSignal.Dispatch();
        }
    }
}