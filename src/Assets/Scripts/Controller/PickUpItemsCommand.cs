﻿using System.Linq;
using Game;
using Game.Map;
using Game.Views.Mediators;
using strange.extensions.command.impl;

namespace Assets.Scripts.Controller
{
    public class PickUpItemsCommand : Command
    {
        [Inject]
        public Scotsman Scotsman { get; set; }

        [Inject]
        public ILevel Level { get; set; }

        [Inject]
        public GameItemPickedUpSignal GameItemPickedUp { get; set; }

        [Inject]
        public GameStatus GameStatus { get; set; }

        public override void Execute()
        {
            var point = Level.Map.First(p => p.Position == Scotsman.Position);
            var item = point.Item;

            if (item is Axe)
                CollectWeapon(point);

            if (item is Coin)
                CollectCoin(point);
        }

        private void AfterPickedUpItem(MapPoint point)
        {
            GameItemPickedUp.Dispatch(point.Item);
            point.Item = null;
        }

        private void CollectWeapon(MapPoint point)
        {
            if (Scotsman.Weapons.Count < Scotsman.MaxNumberOfWeapons)
            {
                var axe = point.Item as Axe;

                if (axe != null)
                {
                    GameStatus.RoundPoints += axe.PointValue;
                }

                Scotsman.Weapons.Add(axe);

                AfterPickedUpItem(point);
            }
        }

        private void CollectCoin(MapPoint point)
        {
            var coin = point.Item as Coin;

            if (coin != null)
            {
                GameStatus.RoundPoints += coin.Value;
                Scotsman.Coins++;
            }

            AfterPickedUpItem(point);
        }
    }
}