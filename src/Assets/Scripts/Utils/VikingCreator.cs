﻿namespace Game.Vikings
{
    public class VikingCreator
    {
        private static int _idSequence;

        public static VikingDto CreateViking()
        {
            return new VikingDto(_idSequence++);
        }
    }

    public static class VikingBuilder
    {
        public static VikingDto WithPosition(this VikingDto item, Position position)
        {
            item.Position = position;
            return item;
        }

        public static VikingDto WithPointValue(this VikingDto item, int value)
        {
            item.Value = value;
            return item;
        }
    }
}