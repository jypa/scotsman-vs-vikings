using System.Collections.Generic;
using System.Linq;
using Game.Map;
using UnityEngine;

namespace Game.Views
{
    public static class ViewHelpers
    {
        public static void DestroyGameObjects(List<GameObject> gameObjects)
        {
            if (gameObjects == null) return;
            foreach (var o in gameObjects)
            {
                Object.Destroy(o);
            }
        }

        public static void DestroyGameObjects(Dictionary<Position, GameObject> gameObjects)
        {
            if (gameObjects == null) return;
            foreach (var item in gameObjects)
            {
                Object.Destroy(item.Value);
            }
        }

        public static void SetSpriteAlpha(GameObject gameobject, float alpha)
        {
            var color = gameobject.GetComponent<SpriteRenderer>().color;
            color.a = alpha;
            gameobject.GetComponent<SpriteRenderer>().color = color;
        }

        public static void SetOrderInLayer(GameObject gameobject, int order)
        {
            gameobject.GetComponent<SpriteRenderer>().sortingOrder = order;
        }
    }
}