﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Assets.Scripts.Utils
{
    public static class Helpers
    {
        public static T[] EnumerateEnum<T>()
        {
            return (T[]) Enum.GetValues(typeof (T));
        }


        public static IEnumerable<Enum> EnumerateEnumAsEnums<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<Enum>();
        }
    }
}