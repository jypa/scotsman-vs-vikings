﻿namespace Game
{
    public static class Movement
    {
        public static Position GetNewPosition(Position position, GameAction gameAction)
        {
            var newPosition = new Position(position.X, position.Y);

            switch (gameAction)
            {
                case GameAction.MoveUp:
                    newPosition.Y++;
                    break;
                case GameAction.MoveRight:
                    newPosition.X++;
                    break;
                case GameAction.MoveDown:
                    newPosition.Y--;
                    break;
                case GameAction.MoveLeft:
                    newPosition.X--;
                    break;
            }

            return newPosition;
        }


        public static GameAction? GetDirection(Position position, Position position2)
        {
            if (position2.Y > position.Y)
                return GameAction.MoveUp;

            if (position2.X > position.X)
                return GameAction.MoveRight;

            if (position2.Y < position.Y)
                return GameAction.MoveDown;

            if (position2.X < position.X)
                return GameAction.MoveLeft;

            return null;
        }
    }
}