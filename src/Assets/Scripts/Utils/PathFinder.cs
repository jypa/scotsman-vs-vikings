﻿using System.Collections.Generic;
using System.Linq;
using Game;
using Game.Map;

namespace Assets.Scripts.Utils
{
    public class PathFinder
    {
        private readonly List<VikingDto> _vikings;
        private readonly int _distanceLimit;
        private readonly ILevel _level;
        private readonly Dictionary<Position, int> _distances;
        private readonly int _roadCount;

        public PathFinder(ILevel level, Position scotsmanPosition, List<VikingDto> vikings, int distanceLimit)
        {
            _level = level;
            _roadCount = level.Map.Count(x => x.Type == MapPointType.Road);
            _vikings = vikings;
            _distanceLimit = distanceLimit;
            _distances = new Dictionary<Position, int> {{ scotsmanPosition, 0 }};
            CalculateDistances();
        }

        private void CalculateDistances()
        {
            var currentDistance = 0;

            while (true)
            {
                var nextPoints = new List<PathfindPoint>();
                foreach (var pathfindPoint in _distances)
                {
                    nextPoints.AddRange(FindNextPoints(currentDistance, pathfindPoint.Key));
                }
                
                AddOrUpdateDistances(nextPoints);

                // Path to all vikings found
                if (!_vikings.Select(x => x.Position).Except(_distances.Keys).Any()) return;

                // Path to all Level points found
                if (_distances.Count >= _roadCount) return;

                // Distance limit 
                if (currentDistance++ > _distanceLimit) return;
            }
        }

        private void AddOrUpdateDistances(List<PathfindPoint> nextPoints)
        {
            foreach (var nextPoint in nextPoints)
            {
                if (_distances.ContainsKey(nextPoint.Position))
                {
                    if (_distances[nextPoint.Position] > nextPoint.Distance)
                    {
                        _distances[nextPoint.Position] = nextPoint.Distance;
                    }
                }
                else
                {
                    _distances.Add(nextPoint.Position, nextPoint.Distance);
                }

            }
        }

        private IEnumerable<PathfindPoint> FindNextPoints(int currentDistance, Position position)
        {
            return _level.GetNeighboringRoads(position).Select(x => new PathfindPoint(x, currentDistance));
        }

        public Position GetNextMove(Position from)
        {
            var neighboringRoads = _level.GetNeighboringRoads(from);

            var position =
                _distances.Where(x => neighboringRoads.Contains(x.Key)).OrderBy(x => x.Value).FirstOrDefault().Key;

            return position;
        }
    }

    internal class PathfindPoint
    {
        public PathfindPoint(Position position, int distance)
        {
            Position = position;
            Distance = distance;
        }

        public Position Position { get; set; }
        public int Distance { get; set; }
    }
}