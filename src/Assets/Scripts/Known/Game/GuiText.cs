﻿namespace Game
{
    public class GuiText
    {
        public GuiTextType Type { get; set; } 
        public Position Position { get; set; }
        public string Text { get; set; }

        public GuiText(GuiTextType type, Position position, string text)
        {
            Type = type;
            Position = position;
            Text = text;
        }
    }
}