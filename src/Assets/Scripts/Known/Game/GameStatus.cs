﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Known.Powerup;
using Game.Map;

namespace Game
{
    public class GameStatus
    {
        [Inject]
        public GameParameters GameParameters { get; set; }

        public bool RoundActive { get; set; }
        public int RoundPoints { get; set; }
        public int RoundTotal { get; set; }
        public int Experience { get; set; }
        public int LastLevel { get; set; }
        public int NextLevel { get; set; }
        public int Level { get; set; }
        public int BestRound { get; set; }
        public bool Victory { get; set; }
        public bool LevelUp { get; set; }
        public bool PowerUpUnlock { get; set; }
        public bool MapUnlocked { get; set; }

        public PowerUp? ActivePowerUp { get; set; }
        public NegativePowerUp? ActiveNegativePowerUp { get; set;}
        
        public List<KeyValuePair<Bonus, int>> RoundBonuses { get; set; }
        public string Map { get; set; }

        public List<Enum> UnlockedPowerUps { get; set; }
        public List<string> UnlockedMaps { get; set; }

        public GameStatus()
        {
            Level = 1;
            RoundPoints = 0;
            BestRound = 0;
            RoundActive = false;
            Victory = false;

            RoundBonuses = new List<KeyValuePair<Bonus, int>>();
            UnlockedPowerUps = new List<Enum>();

            // Unlock first map
            UnlockedMaps = new List<String> {MapStorage.Maps.First().Key};
        }

        [PostConstruct]
        public void Init()
        {
            NextLevel = GameParameters.LevelCap;
            LastLevel = 0;
        }

        public void StartNewRound()
        {
            RoundPoints = 0;
            RoundActive = true;
            Victory = false;
            RoundBonuses.Clear();
        }
    }
}


