﻿namespace Game
{
    public enum Bonus
    {
        Victory,
        PowerUpPenalty,
        NegativePowerUpBonus
    }
}