﻿using System;

namespace Game
{
    public class GameParameters
    {
        // Points
        public int PointsPerCoin = 10;
        public int PointsPerWeapon = 50;
        public int PointsPerKill = 100;

        public int LevelFinishBonus = 500;

        // Pick prime number * 10
        public int LevelCap = 710;

        // Respawn &  Stun
        public int NormalRespawn = 7;
        public int SlowRespawn = 10;
        public int FastRespawn = 5;
        

        // Less vikings = Viking count - 2 * Delta
        // Normal vikings = Viking count - Delta
        // More vikings = Viking count
        public int VikingCountDelta = 1;

        // Power Ups
        public double PowerUpPenaltyPercent = 0;
        public double NegativePowerUpBonusPercent = 0.25;

        // Don't kill all adjecent vikings negative powerup
        public int KillVikingsCount = 1;

        // Dont' kill all & Only stun negative powerup
        public int VikingStunPeriod = 2;

        // Stun all vikings and delay respawn
        public int AllStunPeriod = 3;
        public int DelayRespawnPeriod = 3;

        // Better range
        public int BetterWeaponRange = 2;

        // No turn based power up
        public double RealTimeDelay = 400;

        // Unlock new power up every n:th lvl
        public int PowerUpUnlockLevel = 3;

        // Unlock map every n:th lvl
        public int MapUnlockLevel = 5;

        public int ComboMultiplierAlgorithm(int comboKills)
        {
            return Convert.ToInt32(Math.Pow(2, comboKills));
        }
    }
}