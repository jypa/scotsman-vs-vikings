﻿namespace Game
{
    public enum TimerControl
    {
        Start,
        Stop,
        Reset
    }
}