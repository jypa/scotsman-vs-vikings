﻿namespace Game
{
    public enum GuiState
    {
        PowerUpSelect,
        Round,
        AfterRound,
        MapSelect,
        PowerUpUnlock
    }
}