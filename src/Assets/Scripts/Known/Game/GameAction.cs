﻿namespace Game
{
    public enum GameAction
    {
        MoveUp,
        MoveRight,
        MoveDown,
        MoveLeft,
        Pause
    }
}