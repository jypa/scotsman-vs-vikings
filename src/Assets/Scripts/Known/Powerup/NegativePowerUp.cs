﻿namespace Assets.Scripts.Known.Powerup
{
    public enum NegativePowerUp
    {
        HardMode,
        BluntWeapon,
        FewerWeapons,
        DontKillAllVikings,
        RealTime,
        FogOfWar,
        PitchBlack
    }
}