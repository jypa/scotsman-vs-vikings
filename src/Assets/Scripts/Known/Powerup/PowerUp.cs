﻿namespace Assets.Scripts.Known.Powerup
{
    public enum PowerUp
    {
        EasyMode,
        MoreWeapons,
        WeaponRange,
        StunningWeapon
    }
}