using System;
using System.Collections.Generic;
using System.Linq;

namespace Game.Map
{
    public class Level : ILevel
    {
        public List<MapPoint> Map { get; set; }

        public int CoinCount { get; set; }

        public bool CanWalk(Position newPosition)
        {
            var point = Map.SingleOrDefault(p => p.Position == newPosition);

            return point != null && point.Type == MapPointType.Road;
        }

        public List<Position> GetNeighboringRoads(Position position)
        {
            return GetNeighboringRoads(position, 1);
        }

        public List<Position> GetNeighboringRoads(Position position, int length)
        {
            var positions = new List<Position>();

            GetRoadsAtDirection(position, positions, GameAction.MoveUp, length);
            GetRoadsAtDirection(position, positions, GameAction.MoveRight, length);
            GetRoadsAtDirection(position, positions, GameAction.MoveDown, length);
            GetRoadsAtDirection(position, positions, GameAction.MoveLeft, length);

            return positions;
        }

        public MapPoint GetPointAt(Position position, Direction direction)
        {
            var newPosition = new Position(position);

            switch (direction)
            {
                case Direction.Up:
                    newPosition.Y += 1;
                    return GetPoint(newPosition);
                case Direction.Right:
                    newPosition.X += 1;
                    return GetPoint(newPosition);
                case Direction.Down:
                    newPosition.Y -= 1;
                    return GetPoint(newPosition);
                case Direction.Left:
                    newPosition.X -= 1;
                    return GetPoint(newPosition);
            }

            throw new ArgumentOutOfRangeException("direction");
        }

        private void GetRoadsAtDirection(Position position, ICollection<Position> positions, GameAction direction, int length)
        {
            while (true)
            {
                if (length-- <= 0) return;

                var newPosition = Movement.GetNewPosition(position, direction);
                if (!CanWalk(newPosition)) return;

                positions.Add(newPosition);
                position = newPosition;
            }
        }

        public MapPoint GetPoint(Position position)
        {
            return Map.FirstOrDefault(p => p.Position == position);
        }

        public List<Position> GetNeighboringGrass(Position position)
        {
            var positions = new List<Position>();
            GetGrassAt(positions, position.X, position.Y + 1);
            GetGrassAt(positions, position.X + 1, position.Y + 1);
            GetGrassAt(positions, position.X + 1, position.Y);
            GetGrassAt(positions, position.X + 1, position.Y - 1);
            GetGrassAt(positions, position.X, position.Y - 1);
            GetGrassAt(positions, position.X - 1, position.Y - 1);
            GetGrassAt(positions, position.X - 1, position.Y);
            GetGrassAt(positions, position.X - 1, position.Y + 1);
            return positions;
        }

        private void GetGrassAt(List<Position> positions, int x, int y)
        {
            var newPosition = new Position(x, y);
            var point = GetPoint(newPosition);
            if (point != null && point.Type == MapPointType.Grass) positions.Add(newPosition);
        }


    }
}

