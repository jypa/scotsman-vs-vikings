namespace Game.Map
{
    public enum Direction
    {
        Up,
        Right,
        Down,
        Left
    }
}