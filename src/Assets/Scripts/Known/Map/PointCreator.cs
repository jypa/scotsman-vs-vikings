﻿using System;
using System.Collections.Generic;
using System.Linq;
using Game.Map;

namespace Game
{
    public interface IPointCreator
    {
        MapPoint CreatePoint(Position position, char c);
    }

    public class PointCreator : IPointCreator
    {
        [Inject("CoinFactory")]
        public IItemFactory CoinFactory { get; set; }

        [Inject("WeaponFactory")]
        public IItemFactory WeaponFactory { get; set; }

        private Dictionary<char, PointInformation> _mapGenerationPointTypes;

        [PostConstruct]
        public void Init()
        {
            _mapGenerationPointTypes = new Dictionary<char, PointInformation> {
                {'G', new PointInformation(MapGenerationPointType.Grass, MapPointType.Grass)},
                {'R', new PointInformation(MapGenerationPointType.Road, MapPointType.Road)},
                {'*', new PointInformation(MapGenerationPointType.Coin, MapPointType.Road, CoinFactory)},
                {'S', new PointInformation(MapGenerationPointType.Scotsman, MapPointType.Road)},
                {'V', new PointInformation(MapGenerationPointType.Viking, MapPointType.Road)},
                {'W', new PointInformation(MapGenerationPointType.Weapon, MapPointType.Road, WeaponFactory)}
             };
        }

        public MapPoint CreatePoint(Position position, char c)
        {
            var generationPointType = ResolveGenerationType(c);
            var pointType = ResolvePointType(generationPointType);
            var item = GetItem(generationPointType);

            return new MapPoint(position, generationPointType, pointType, item);
        }

        private MapGenerationPointType ResolveGenerationType(char c)
        {
            if (_mapGenerationPointTypes.ContainsKey(c))
            {
                return _mapGenerationPointTypes[c].GenerationPointType;
            }

            throw new ArgumentException(string.Format("Couldn't resolve type for '{0}'", c));
        }

        private MapPointType ResolvePointType(MapGenerationPointType generationPointType)
        {
            var pointPair =
                _mapGenerationPointTypes.FirstOrDefault(x => x.Value.GenerationPointType == generationPointType).Value;

            if (pointPair != null)
            {
                return pointPair.MapPointType;
            }

            throw new ArgumentException(string.Format("Couldn't resolve type for '{0}'", generationPointType));
        }

        private GameItem GetItem(MapGenerationPointType generationPointType)
        {
            var pointPair =
                _mapGenerationPointTypes.FirstOrDefault(x => x.Value.GenerationPointType == generationPointType).Value;

            if (pointPair == null) throw new ArgumentException(string.Format("Couldn't resolve type for '{0}'", generationPointType));


            return pointPair.GetItem();
        }
    }

    internal class PointInformation
    {
        public MapGenerationPointType GenerationPointType { get; set; }
        public MapPointType MapPointType { get; set; }
        public IItemFactory ItemFactory { get; set; }

        public PointInformation(MapGenerationPointType generationPointType, MapPointType mapPointType)
        {
            GenerationPointType = generationPointType;
            MapPointType = mapPointType;
        }

        public PointInformation(MapGenerationPointType generationPointType, MapPointType mapPointType, IItemFactory itemFactory)
        {
            GenerationPointType = generationPointType;
            MapPointType = mapPointType;
            ItemFactory = itemFactory;
        }

        public GameItem GetItem()
        {
            return ItemFactory != null ? ItemFactory.CreateItem() : null;
        }
    }
}