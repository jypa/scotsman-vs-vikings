namespace Game.Map
{
    public class MapPoint
    {
        public Position Position { get; set; }
        public MapGenerationPointType GenerationPointType { get; set; }
        public MapPointType Type { get; set; }
        public GameItem Item { get; set; }
        public bool Visited { get; set; }
        public bool Visible { get; set; }

        public MapPoint(Position position, MapGenerationPointType generationPointType, MapPointType type, GameItem item)
        {
            Position = position;
            GenerationPointType = generationPointType;
            Type = type;
            Item = item;
            Visible = true;

            // Item needs referece to it's parent
            if (item != null)
            {
                Item.MapPoint = this;
            }
        }
    }
}

