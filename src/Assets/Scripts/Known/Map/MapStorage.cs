﻿using System.Collections.Generic;

namespace Game.Map
{
    public static class MapStorage
    {
        public static Dictionary<string, string[]> Maps { get; private set; }

        static MapStorage()
        {
            Maps = new Dictionary<string, string[]>();

            Maps.Add("Level 1", new[]
            {
                "GGGGGGGGGGGGGGGGGGGGG",
                "GGV***G**V*V**G****GG",
                "GG*GG*G*GGGGG*G*GG*GG",
                "GGW*******G******VWGG",
                "GG*GG*GGG*G*GGG*GG*GG",
                "GG**G**W**SR*W**G**GG",
                "GGG*G*G*GGGGG*G*G*GGG",
                "GG***WG***G***GW***GG",
                "GG*GGGGGG*G*GGGGGG*GG",
                "GGVW*************WVGG",
                "GGGGGGGGGGGGGGGGGGGGG"
            });

            Maps.Add("Level 2", new[]
            {
                "GGGGGGGGGGGGGGGGGGGGG",
                "GGGGGGGGGGVGGGGGGGGGG",
                "GG****G*******G****GG",
                "GG*GG*G*GGGGG*G*GG*GG",
                "GG*****W*****W*****GG",
                "GG*GG*GGGG*GGGG*GG*GG",
                "GV*WG*****S*****GW*VG",
                "GGG*G*GGG*G*GGG*G*GGG",
                "GG********G********GG",
                "GG*GGGGGWGGGWGGGGG*GG",
                "GG*****************GG",
                "GGGGGGGGGGVGGGGGGGGGG",
                "GGGGGGGGGGGGGGGGGGGGG"
            });

            Maps.Add("Level 3", new[]
            {
                "GGGGGGGGGGGGGGGGGGGGG",
                "GGGGGGGGGGGGGGGGGGGGG",
                "GGV***************VGG",
                "GG*GGWGG*GGG*GGWGG*GG",
                "GG*****************GG",
                "GGG*GGG*GGGGG*GGG*GGG",
                "GGG*****W*S*W*****GGG",
                "GGG*GGGG*GGG*GGGG*GGG",
                "GG*****************GG",
                "GG*GGWGG*GGG*GGWGG*GG",
                "GGV***************VGG",
                "GGGGGGGGGGGGGGGGGGGGG",
                "GGGGGGGGGGGGGGGGGGGGG"
            });

            Maps.Add("Level 4", new[]
            {
                "GGGGGGGGGGGGGGGGGGGGG",
                "GGGGGGGGGGGGGGGGGGGGG",
                "GGV***************VGG",
                "GG*GGGGWGG*GGWGGGG*GG",
                "GG*G*****G*G*****G*GG",
                "GG*G*GGG*G*G*GGG*G*GG",
                "GG*W******S******W*GG",
                "GG*G*GGG*G*G*GGG*G*GG",
                "GG*G*****G*G*****G*GG",
                "GG*GGWGGGG*GGGGWGG*GG",
                "GGV***************VGG",
                "GGGGGGGGGGGGGGGGGGGGG",
                "GGGGGGGGGGGGGGGGGGGGG"
            });

            Maps.Add("Bonus", new[]
            {
                "GGGGGGGGGGGGGGGGGGGGG",
                "GGV*******G*******VGG",
                "GG*GG*GGG*G*GGG*GG*GG",
                "GG********W********GG",
                "GG*GG*G*GGGGG*G*GG*GG",
                "GG****G***G***G****GG",
                "GGGGG*GGG*G*GGG*GGGGG",
                "GGGGG*G*******G*GGGGG",
                "GGGGG*G*GGGGG*G*GGGGG",
                "GW******GGGGG******WG",
                "GGGGG*G*GGGGG*G*GGGGG",
                "GGGGG*G*******G*GGGGG",
                "GGGGG*G*GGGGG*G*GGGGG",
                "GG********G********GG",
                "GG*GG*GGG*G*GGG*GG*GG",
                "GG**G*****S*****G**GG",
                "GGG*G*G*GGGGG*G*G*GGG",
                "GG****G***G***G****GG",
                "GGVGGGGGG*G*GGGGGGVGG",
                "GG********W********GG",
                "GGGGGGGGGGGGGGGGGGGGG"
            });
        }
    }
}