using System.Collections.Generic;

namespace Game.Map
{
	public interface ILevel
	{
        List<MapPoint> Map { get; set; }
        int CoinCount { get; set; }
        bool CanWalk(Position newPosition);
	    List<Position> GetNeighboringRoads(Position position);
        MapPoint GetPoint(Position position);
	    List<Position> GetNeighboringGrass(Position position);
        List<Position> GetNeighboringRoads(Position position, int length);
	    MapPoint GetPointAt(Position position, Direction direction);
	}
}

