namespace Game
{
    public interface IItemFactory
    {
        GameItem CreateItem();
    }
}