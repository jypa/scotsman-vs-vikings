﻿namespace Game
{
    public class Axe : GameItem
    {
        public int PointValue{ get; set; }
        public int Reach { get; set; }

        public Axe(int pointValue, int reach)
        {
            PointValue = pointValue;
            Reach = reach;
        }
    }
}