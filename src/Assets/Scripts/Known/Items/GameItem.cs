using Game.Map;

namespace Game
{
    public class GameItem
    {
        public MapItemType ItemType { get; set; }
        public MapPoint MapPoint { get; set; }
    }
}