﻿using Assets.Scripts.Known.Powerup;

namespace Game
{
    public class WeaponFactory : IItemFactory
    {
        [Inject]
        public GameParameters GameParameters { get; set; }

        [Inject]
        public GameStatus GameStatus { get; set; }

        public GameItem CreateItem()
        {
            var reach = 1;

            if (GameStatus.ActivePowerUp == PowerUp.WeaponRange)
            {
                reach = GameParameters.BetterWeaponRange;
            }

            return new Axe(GameParameters.PointsPerWeapon, reach);
        }
    }
}