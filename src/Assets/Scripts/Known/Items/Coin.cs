namespace Game
{
    public class Coin : GameItem
    {
        public int Value { get; set; }

        public Coin(int value)
        {
            Value = value;
        }
    }
}