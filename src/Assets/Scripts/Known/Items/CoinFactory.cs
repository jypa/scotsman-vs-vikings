﻿using Game.Views;

namespace Game
{
    public class CoinFactory : IItemFactory
    {
        [Inject]
        public GameParameters GameParameters { get; set; }

        public GameItem CreateItem()
        {
            return new Coin(GameParameters.PointsPerCoin);
        }
    }
}