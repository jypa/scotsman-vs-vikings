﻿namespace Game
{
    public class RespawnTimer
    {
        public int RoundCount { get; set; }

        public RespawnTimer(int roundCount)
        {
            RoundCount = roundCount;
        }
    }
}