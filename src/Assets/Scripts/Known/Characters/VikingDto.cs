﻿namespace Game
{
    public class VikingDto
    {
        public int Id { get; private set; }
        public Position Position { get; set; }
        public int Value { get; set; }
        public int Stunned { get; set; }

        public VikingDto(int id)
        {
            Id = id;
        }
    }
}