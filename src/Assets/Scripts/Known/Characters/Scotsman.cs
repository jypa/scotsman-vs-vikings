﻿using System.Collections.Generic;

namespace Game
{
    public class Scotsman
    {
        public Position Position { get; set; }
        public List<Axe> Weapons { get; set; }

        public int Coins { get; set; }
        public int ComboMultiplier { get; set; }
        public int MaxNumberOfWeapons { get; set; }

        public Scotsman()
        {
            Weapons = new List<Axe>();
        }
    }
}