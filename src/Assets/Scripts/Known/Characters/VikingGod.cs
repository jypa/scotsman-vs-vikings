﻿using System.Collections.Generic;

namespace Game
{
    public interface IVikingGod
    {
        List<VikingDto> Vikings { get; set; }
        List<RespawnTimer> Respawns { get; set; }
        void Init();
    }

    public class VikingGod : IVikingGod
    {
        public List<VikingDto> Vikings { get; set; }
        public List<RespawnTimer> Respawns { get; set; }

        public void Init()
        {
            Vikings = new List<VikingDto>();
            Respawns = new List<RespawnTimer>();
        }
    }
}