﻿using strange.extensions.signal.impl;

namespace Game.Views.Mediators
{
    public class GameItemPickedUpSignal : Signal<GameItem>
    {
    }
}