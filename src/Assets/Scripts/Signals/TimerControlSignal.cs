﻿using strange.extensions.signal.impl;

namespace Game.Views.Mediators
{
    public class TimerControlSignal : Signal<TimerControl>
    {
    }
}