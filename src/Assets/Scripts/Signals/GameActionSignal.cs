using strange.extensions.signal.impl;

namespace Game.Signals
{
    public class GameActionSignal : Signal<GameAction>
    {
    }
}