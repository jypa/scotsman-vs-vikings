using Assets.Scripts.Controller;
using Assets.Scripts.Signals;
using Game.Map;
using Game.Signals;
using Game.Views;
using Game.Views.Mediators;
using strange.extensions.command.api;
using strange.extensions.command.impl;
using strange.extensions.context.impl;
using UnityEngine;

namespace Game.Start
{
    public class GameContext : MVCSContext
    {
        public GameContext()
        {
        }

        public GameContext(MonoBehaviour view, bool autoStartup) : base(view, autoStartup)
        {
        }

        protected override void addCoreComponents()
        {
            base.addCoreComponents();
            injectionBinder.Unbind<ICommandBinder>();
            injectionBinder.Bind<ICommandBinder>().To<SignalCommandBinder>().ToSingleton();
        }

        protected override void mapBindings()
        {
            BindViews();
            BindSingletonSignals();
			BindObjects();
            BindCommands();
        }

        override public void Launch()
        {
            base.Launch();
            injectionBinder.GetInstance<RoundStartSignal>().Dispatch();
        }

        private void BindCommands()
        {
            commandBinder.Bind<RoundStartSignal>().InSequence()
                .To<CreateMapCommand>()
                .To<CreateScotsmanCommand>()
                .To<CreateVikingsCommand>()
                .To<RevealLevelCommand>()
                .To<ResetViewsCommand>()
                .To<StartPowerUps>();

            commandBinder.Bind<GameActionSignal>().InSequence()
                .To<ValidateRoundCommand>()
                .To<ScotsmanActionCommand>()
                .To<PickUpItemsCommand>()
                .To<CheckRoundEndCommand>()
                .To<VikingsActionCommand>()
                .To<RespawnVikingsCommand>()
                .To<RevealLevelCommand>()
                .To<CheckCollosionCommand>();

            commandBinder.Bind<AttackSignal>()
                .To<AttackCommand>();

            commandBinder.Bind<GameFlowControlSignal>()
                .To<GameFlowCommand>();

            commandBinder.Bind<RoundEndSignal>()
                .To<RoundEndCommand>();
        }

        private void BindObjects()
        {
            injectionBinder.Bind<ILevel>().To<Level>().ToSingleton();
            injectionBinder.Bind<IVikingGod>().To<VikingGod>().ToSingleton();

            injectionBinder.Bind<IPointCreator>().To<PointCreator>();
            injectionBinder.Bind<IItemFactory>().To<CoinFactory>().ToName("CoinFactory");
            injectionBinder.Bind<IItemFactory>().To<WeaponFactory>().ToName("WeaponFactory");

            injectionBinder.Bind<Scotsman>().ToSingleton();
            injectionBinder.Bind<GameStatus>().ToSingleton();
            injectionBinder.Bind<GameParameters>().ToSingleton();
        }

        private void BindSingletonSignals()
        {
            injectionBinder.Bind<ScotsmanMoveSignal>().ToSingleton();
            injectionBinder.Bind<UpdateVikingSignal>().ToSingleton();
            injectionBinder.Bind<ShowRoundEndGuiSignal>().ToSingleton();
            injectionBinder.Bind<ResetViewsSignal>().ToSingleton();
            injectionBinder.Bind<GameItemPickedUpSignal>().ToSingleton();
            injectionBinder.Bind<KillVikingSignal>().ToSingleton();
            injectionBinder.Bind<UpdateFogOfWarSignal>().ToSingleton();
            injectionBinder.Bind<RespawnVikingSingal>().ToSingleton();
            injectionBinder.Bind<TimerControlSignal>().ToSingleton();
            injectionBinder.Bind<GuiTextSignal>().ToSingleton();
        }

        private void BindViews()
        {
            mediationBinder.Bind<ScotsmanView>().To<ScotsmanMediator>();
            mediationBinder.Bind<MapView>().To<MapMediator>();
            mediationBinder.Bind<CameraView>().To<CameraMediator>();
            mediationBinder.Bind<VikingsView>().To<VikingsMediator>();
            mediationBinder.Bind<GuiView>().To<GuiMediator>();
            mediationBinder.Bind<ItemsView>().To<ItemsMediator>();
            mediationBinder.Bind<TimerView>().To<TimerMediator>();
        }
    }
}

