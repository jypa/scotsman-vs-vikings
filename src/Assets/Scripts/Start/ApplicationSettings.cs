﻿using UnityEngine;

namespace Game.Start
{
    public class ApplicationSettings : MonoBehaviour
    {
        public void Awake()
        {
            Application.targetFrameRate = 60;
        }
    }
}