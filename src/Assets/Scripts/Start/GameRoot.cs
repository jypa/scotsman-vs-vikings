using strange.extensions.context.impl;

namespace Game.Start
{
    public class GameRoot : ContextView
    {
        public void Awake()
        {
            context = new GameContext(this, true);
            context.Start();
        }
    }
}