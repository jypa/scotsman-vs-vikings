﻿using System.Collections.Generic;
using System.Linq;
using Game.Signals;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Game.Views
{
    public class KeyboardInput : View
    {
        private readonly Dictionary<KeyCode, GameAction> _controlMap =
            new Dictionary<KeyCode, GameAction>
            {
                { KeyCode.UpArrow, GameAction.MoveUp },
                { KeyCode.RightArrow, GameAction.MoveRight },
                { KeyCode.DownArrow, GameAction.MoveDown },
                { KeyCode.LeftArrow, GameAction.MoveLeft },
                { KeyCode.Space, GameAction.Pause}
            };

        private readonly Dictionary<KeyCode, GameFlowAction> _commandsMap =
            new Dictionary<KeyCode, GameFlowAction>
            {
                { KeyCode.R, GameFlowAction.Start}
            };

        [Inject]
        public GameActionSignal GameActionSignal { get; set; }

        [Inject]
        public GameFlowControlSignal GameFlowControlSignal { get; set; }

        public void Update()
        {
            foreach (var direction in _controlMap.Where(direction => Input.GetKeyDown(direction.Key)))
            {
                GameActionSignal.Dispatch(direction.Value);
            }

            foreach (var command in _commandsMap.Where(command => Input.GetKeyDown(command.Key)))
            {
                GameFlowControlSignal.Dispatch(command.Value);
            }

        }
    }
}