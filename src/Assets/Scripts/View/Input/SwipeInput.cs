﻿using System;
using Game.Signals;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Game.Views
{
    public class SwipeInput : View
    {
        private float _fingerStartTime;
        private Vector2 _fingerStartPos = Vector2.zero;
        private bool _isSwipe;

        public float MinSwipeDist = 50.0f;
        public float MaxSwipeTime = 0.5f;
        public float MinPauseTime = 0.2f;

        [Inject]
        public GameActionSignal GameActionSignal { get; set; }

        public void Update()
        {
            if (Input.touchCount <= 0) return;

            foreach (var touch in Input.touches)
            {
                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        _isSwipe = true;
                        _fingerStartTime = Time.time;
                        _fingerStartPos = touch.position;
                        break;

                    case TouchPhase.Canceled:
                        _isSwipe = false;
                        break;

                    case TouchPhase.Ended:
                        TouchEnd(touch);
                        break;
                }
            }
        }

        private void TouchEnd(Touch touch)
        {
            if (!_isSwipe) return;

            var gestureTime = Time.time - _fingerStartTime;
            var gestureDist = (touch.position - _fingerStartPos).magnitude;

            if (gestureTime < MaxSwipeTime && gestureDist > MinSwipeDist)
            {
                var direction = touch.position - _fingerStartPos;
                Vector2 swipeType;

                if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
                {
                    // the swipe is horizontal:
                    swipeType = Vector2.right*Mathf.Sign(direction.x);
                }
                else
                {
                    // the swipe is vertical:
                    swipeType = Vector2.up*Mathf.Sign(direction.y);
                }

                if (swipeType.x != 0.0f)
                {
                    GameActionSignal.Dispatch(swipeType.x > 0.0f ? GameAction.MoveLeft : GameAction.MoveRight);
                }

                if (swipeType.y != 0.0f)
                {
                    GameActionSignal.Dispatch(swipeType.y > 0.0f ? GameAction.MoveDown : GameAction.MoveUp);
                }
            }
            else if (gestureTime > MinPauseTime && Math.Abs(gestureDist) < 0.001f)
            {
                GameActionSignal.Dispatch(GameAction.Pause);
            }
        }
    }
}