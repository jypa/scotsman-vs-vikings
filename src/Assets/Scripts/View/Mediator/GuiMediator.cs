﻿using Assets.Scripts.Signals;
using Game.Signals;
using strange.extensions.mediation.impl;

namespace Game.Views.Mediators
{
    public class GuiMediator : Mediator
    {
        [Inject]
        public GuiView View { get; set; }

        [Inject]
        public ResetViewsSignal ResetViewsSignal { get; set; }

        [Inject]
        public ShowRoundEndGuiSignal RoundEndSignal { get; set; }

        [Inject]
        public GuiTextSignal GuiTextSignal { get; set; }

        public override void OnRegister()
        {
            RoundEndSignal.AddListener(OnRoundEnd);
            ResetViewsSignal.AddListener(InitView);
            GuiTextSignal.AddListener(OnGuiText);
            View.Init();
        }

        private void InitView()
        {
            View.GuiState = GuiState.Round;
        }

        public override void OnRemove()
        {
            GuiTextSignal.RemoveListener(OnGuiText);
            RoundEndSignal.RemoveListener(OnRoundEnd);
        }

        private void OnGuiText(GuiText text)
        {
            View.DisplayGuiText(text);
        }

        private void OnRoundEnd()
        {
            View.GuiState = GuiState.AfterRound;
        }
    }
}