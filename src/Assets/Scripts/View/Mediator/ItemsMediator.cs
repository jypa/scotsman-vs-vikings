﻿using Game.Signals;
using strange.extensions.mediation.impl;

namespace Game.Views.Mediators
{
    public class ItemsMediator : Mediator
    {
        [Inject]
        public ItemsView View { get; set; }

        [Inject]
        public GameItemPickedUpSignal GameItemPickedUpSignal { get; set; }

        [Inject]
        public ResetViewsSignal ResetViewsSignal { get; set; }


        public override void OnRegister()
        {
            ResetViewsSignal.AddListener(InitView);
            GameItemPickedUpSignal.AddListener(OnItemPickUp);
            InitView();
        }

        public override void OnRemove()
        {
            ResetViewsSignal.RemoveListener(InitView);
            GameItemPickedUpSignal.RemoveListener(OnItemPickUp);
        }

        private void OnItemPickUp(GameItem gameItem)
        {
            View.DestroyItem(gameItem);
        }

        private void InitView()
        {
            View.DrawItems();
        }
    }
}