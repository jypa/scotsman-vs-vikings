﻿using Assets.Scripts.Signals;
using Game.Signals;
using strange.extensions.mediation.impl;

namespace Game.Views.Mediators
{
    public class VikingsMediator : Mediator
    {
        [Inject]
        public VikingsView View { get; set; }

        [Inject]
        public IVikingGod VikingGod { get; set; }

        [Inject]
        public UpdateVikingSignal UpdateVikingSignal { get; set; }

        [Inject]
        public ResetViewsSignal ResetViewsSignal { get; set; }

        [Inject]
        public KillVikingSignal KillVikingSignal { get; set; }

        [Inject]
        public UpdateFogOfWarSignal UpdateFogOfWarSignal { get; set; }

        [Inject]
        public RespawnVikingSingal RespawnVikingSingal { get; set; }

        public override void OnRegister()
        {
            UpdateVikingSignal.AddListener(OnVikingUpdate);
            ResetViewsSignal.AddListener(InitView);
            KillVikingSignal.AddListener(OnKillViking);
            UpdateFogOfWarSignal.AddListener(OnFogOfWarUpdate);
            RespawnVikingSingal.AddListener(OnVikingRespawn);
            View.AddVikings(VikingGod.Vikings);
        }

        
        public override void OnRemove()
        {
            UpdateVikingSignal.RemoveListener(OnVikingUpdate);
            ResetViewsSignal.RemoveListener(InitView);
            KillVikingSignal.RemoveListener(OnKillViking);
            UpdateFogOfWarSignal.RemoveListener(OnFogOfWarUpdate);
            RespawnVikingSingal.RemoveListener(OnVikingRespawn);
        }

        private void OnVikingRespawn(VikingDto viking)
        {
            View.AddViking(viking);
        }

        private void OnFogOfWarUpdate()
        {
            View.UpdateVikingVisibility();
        }

        private void OnVikingUpdate(VikingDto vikingDto)
        {
            View.MoveViking(vikingDto);
        }

        private void OnKillViking(VikingDto viking)
        {
            View.DestroyViking(viking);
        }

        private void InitView()
        {
            View.AddVikings(VikingGod.Vikings);
        }

    }
}