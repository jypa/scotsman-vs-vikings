﻿using Game.Signals;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Game.Views.Mediators
{
    public class ScotsmanMediator : EventMediator
    {
        [Inject]
        public ScotsmanView View { get; set; }

        [Inject]
        public ScotsmanMoveSignal ScotsmanMoveInputSignal { get; set; }

        [Inject]
        public Scotsman Scotsman { get; set; }

        [Inject]
        public ResetViewsSignal ResetViewsSignal { get; set; }

        [Inject]
        public KillVikingSignal KillVikingSignal { get; set; }

        public override void OnRegister()
        {
            ScotsmanMoveInputSignal.AddListener(OnMove);
            ResetViewsSignal.AddListener(InitView);
            KillVikingSignal.AddListener(OnKillViking);
            InitView();
            View.Animator = GetComponent<Animator>();
        }

        private void OnKillViking(VikingDto viking)
        {
            View.PlayAttackAnimation();
        }

        public override void OnRemove()
        {
            ScotsmanMoveInputSignal.RemoveListener(OnMove);
            ResetViewsSignal.RemoveListener(InitView);
            KillVikingSignal.RemoveListener(OnKillViking);
        }

        private void InitView()
        {
            View.Jump(Scotsman.Position);
        }

        private void OnMove(Position position)
        {
            View.Move(position);
        }
    }
}