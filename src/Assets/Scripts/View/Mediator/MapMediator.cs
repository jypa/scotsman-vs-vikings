﻿using Game.Signals;
using strange.extensions.mediation.impl;

namespace Game.Views.Mediators
{
    public class MapMediator : Mediator
    {
        [Inject] 
        public MapView View { get; set; }

        [Inject]
        public ResetViewsSignal ResetViewsSignal { get; set; }

        [Inject]
        public UpdateFogOfWarSignal UpdateFogOfWarSignal { get; set; }

        public override void OnRegister()
        {
            ResetViewsSignal.AddListener(InitView);
            UpdateFogOfWarSignal.AddListener(OnFogOfWarUpdate);
            InitView();
        }

        public override void OnRemove()
        {
            ResetViewsSignal.RemoveListener(InitView);

            UpdateFogOfWarSignal.RemoveListener(OnFogOfWarUpdate);
        }

        private void OnFogOfWarUpdate()
        {
            View.DrawFogOfWar();
        }

        private void InitView()
        {
            View.DrawTiles();
            View.DrawFogOfWar();
        }
    }
}