﻿using Game.Signals;
using strange.extensions.mediation.impl;

namespace Game.Views.Mediators
{
    public class CameraMediator : Mediator
    {
        [Inject]
        public CameraView View { get; set; }

        [Inject]
        public ScotsmanMoveSignal ScotsmanMoveInputSignal { get; set; }

        [Inject]
        public ResetViewsSignal ResetViewsSignal { get; set; }

        [Inject]
        public Scotsman Scotsman { get; set; }

        public override void OnRegister()
        {
            ScotsmanMoveInputSignal.AddListener(OnMove);
            ResetViewsSignal.AddListener(InitView);
            InitView();
        }

        public override void OnRemove()
        {
            ScotsmanMoveInputSignal.RemoveListener(OnMove);
            ResetViewsSignal.RemoveListener(InitView);
        }

        private void InitView()
        {
            View.Jump(Scotsman.Position);
        }

        private void OnMove(Position position)
        {
            View.Move(position);
        }
    }
}