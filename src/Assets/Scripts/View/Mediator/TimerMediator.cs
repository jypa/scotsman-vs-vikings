﻿using strange.extensions.mediation.impl;

namespace Game.Views.Mediators
{
    public class TimerMediator : Mediator
    {
        [Inject]
        public TimerView View { get; set; }

        [Inject]
        public TimerControlSignal TimerControlSignal { get; set; }

        public override void OnRegister()
        {
            TimerControlSignal.AddListener(OnTimerControl);
        }

        public override void OnRemove()
        {
            TimerControlSignal.RemoveListener(OnTimerControl);
            View.StopTicker();
        }

        private void OnTimerControl(TimerControl action)
        {
            switch (action)
            {
                case TimerControl.Start:
                    View.StartTicker();
                    break;

                case TimerControl.Reset:
                    View.ResetTicker();
                    break;

                case TimerControl.Stop:
                    View.StopTicker();
                    break;
            }
        }
    }
}