﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Known.Powerup;
using Assets.Scripts.Utils;
using Game.Map;
using Game.Signals;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Game.Views
{
    public class GuiView : View
    {
        public GUIStyle PlayButton;
        public GUIStyle PowerUpButton;
        public GUIStyle DisabledPowerUpButton;
        public GUIStyle MainTitle;
        public GUIStyle SecondTitle;
        public GUIStyle ThirdTitle;
        public GUIStyle Box;
        public GUIStyle TextStyle;
        public GUIStyle PointsTexts;
        public GUIStyle ScoreTexs;

        public Texture2D Coin;

        public float ShowGuiTextTime = 0.5f;

        [Inject]
        public Scotsman Scotsman { get; set; }

        [Inject]
        public ILevel Level { get; set; }

        [Inject]
        public GameFlowControlSignal GameFlowControlSignal { get; set; }

        [Inject]
        public GameStatus GameStatus { get; set; }

        [Inject]
        public GameParameters GameParameters { get; set; }

        private const int MaxWidth = 2000;
        private const int MaxHeight = 2000;
        private const int GuiMargin = 100;
        private const int ButtonWidth = 200;
        private const int ButtonHeight = 60;
        private const int TextHeight = 35;

        private const int Padding = 20;
        private const int TitleHeight = 90;

        private readonly int _width = Math.Min(MaxWidth, Screen.width - GuiMargin);
        private readonly int _height = Math.Min(MaxHeight, Screen.height - GuiMargin);

        private readonly Dictionary<Enum, string> _guiTextsTranslations = new Dictionary<Enum, string>
        {
            {PowerUp.EasyMode, "Easy Mode" },
            {PowerUp.MoreWeapons, "Carry more weapons" },
            {PowerUp.StunningWeapon, "Stunning weapon" },
            {PowerUp.WeaponRange, "Better weapon range" },
            {NegativePowerUp.HardMode, "Hard Mode" },
            {NegativePowerUp.BluntWeapon, "Blunt weapon" },
            {NegativePowerUp.FewerWeapons, "Carry less weapons" },
            {NegativePowerUp.DontKillAllVikings, "Kill only one" },
            {NegativePowerUp.RealTime, "No turn based" },
            {NegativePowerUp.FogOfWar, "Fog of War" },
            {NegativePowerUp.PitchBlack, "Pitch black" },
            {Bonus.Victory, "Round finish bonus" },
            {Bonus.PowerUpPenalty, "PowerUp penalty" },
            {Bonus.NegativePowerUpBonus, "Negative-PowerUp bonus" }
        };

        private Dictionary<Enum, bool> _selectedPowerUp;
        private Dictionary<string, bool> _selectedMap;

        private readonly List<KeyValuePair<float, GuiText>> _guiTexts = new List<KeyValuePair<float, GuiText>>();


        public GuiState GuiState { get; set; }

        public void OnGUI()
        {
            switch (GuiState)
            {
                case GuiState.MapSelect:
                    MapSelectGui();
                    break;
                case GuiState.PowerUpUnlock:
                    PowerUpUnlockGui();
                    break;
                case GuiState.PowerUpSelect:
                    PowerUpSelectGui();
                    break;
                case GuiState.AfterRound:
                    AfterRoundGui();
                    break;
                case GuiState.Round:
                    ScoreGui();
                    break;
            }
        }


        private void UnlockPowerUp(bool toggle, Enum powerUp)
        {
            // Check if power up is already unlocked
            if (GameStatus.UnlockedPowerUps.Contains(powerUp)) return;

            if (!toggle) return;

            ClearPowerUps();
            _selectedPowerUp[powerUp] = true;
        }

        private void MapSelectGui()
        {
            var middleHeight = _height - 2 * Padding - TitleHeight - ButtonHeight;

            GUI.BeginGroup(new Rect(Screen.width / 2 - _width / 2, Screen.height / 2 - _height / 2, _width, _height));
            GUI.Box(new Rect(0, 0, _width, _height), "", Box);

            DrawMainTitle();

            const int secondTitleWidth = 230;
            GUI.Label(new Rect(_width / 2 - secondTitleWidth / 2, TitleHeight + 2 * Padding, secondTitleWidth, TitleHeight + Padding), "Select map", SecondTitle);

            GUI.BeginGroup(new Rect(Padding, 2 * TitleHeight + 2 * Padding, _width - 2 * Padding, middleHeight));

            var mapSelectWidth = _width - 2 * Padding;
            var row = 0;
            foreach (var map in MapStorage.Maps)
            {
                SelectMap(GUI.Toggle(new Rect(0, row++ * TextHeight, mapSelectWidth, TextHeight), _selectedMap[map.Key], map.Key, ResolveMapToggleStyle(map.Key)), map.Key);
            }

            GUI.EndGroup();

            if (GUI.Button(new Rect(_width - ButtonWidth - Padding, _height - ButtonHeight - Padding, ButtonWidth, ButtonHeight), "Next", PlayButton))
            {
                GuiState = GuiState.PowerUpSelect;
            }

            GUI.EndGroup();
        }

        private GUIStyle ResolveMapToggleStyle(string mapName)
        {
            return GameStatus.UnlockedMaps.Contains(mapName) ? PowerUpButton : DisabledPowerUpButton;
        }

        private void SelectMap(bool toggle, string mapName)
        {
            if (!toggle || !GameStatus.UnlockedMaps.Contains(mapName)) return;

            foreach (var map in MapStorage.Maps)
            {
                _selectedMap[map.Key] = false;
            }

            _selectedMap[mapName] = true;
        }

        private void ScoreGui()
        {
            DrawRoundStats();

            DrawKillPoints();
        }

        private void DrawRoundStats()
        {
            GUI.BeginGroup(new Rect(10, 10, 400, 400));
            //GUILayout.Label(string.Format("{0}/{1} coins", Scotsman.Coins, Level.CoinCount), ScoreTexs);

            GUILayout.Label(string.Format("{0} points", GameStatus.RoundPoints), ScoreTexs);

            GUILayout.Label(string.Format("{0}/{1} weapons", Scotsman.Weapons.Count, Scotsman.MaxNumberOfWeapons), ScoreTexs);
            GUI.EndGroup();
        }

        private void DrawKillPoints()
        {
            foreach (var text in _guiTexts.Select(x => x.Value))
            {
                var worldPoint = text.Position.GetVector2();

                if (Scotsman.Position.Y < text.Position.Y)
                {
                    worldPoint.y -= 2;
                }

                if (Scotsman.Position.Y > text.Position.Y)
                {
                    worldPoint.y += 2;
                }

                var position = Camera.main.WorldToScreenPoint(worldPoint);
                GUI.Label(new Rect(position.x - 32, position.y - 70, 64, 30),
                    text.Text,
                    PointsTexts);
            }

            // Clear old texts
            _guiTexts.RemoveAll(x => x.Key < Time.time);
        }

        private void AfterRoundGui()
        {
            var middleHeight = _height - 2 * Padding - TitleHeight - ButtonHeight;

            GUI.BeginGroup(new Rect(Screen.width / 2 - _width / 2, Screen.height / 2 - _height / 2, _width, _height));
            GUI.Box(new Rect(0, 0, _width, _height), "", Box);

            DrawMainTitle();

            var roundFinishText = GameStatus.Victory ? "Level cleared." : "You died! Better luck next time.";

            const int secondTitleWidth = 230;
            GUI.Label(new Rect(_width / 2 - secondTitleWidth / 2, TitleHeight + 2 * Padding, secondTitleWidth, TitleHeight + Padding), roundFinishText, SecondTitle);

            var width = _width - 2 * Padding;

            GUI.BeginGroup(new Rect(Padding, 2 * TitleHeight + 3 * Padding, width, middleHeight));

            var row = 0;

            if (GameStatus.RoundPoints != GameStatus.RoundTotal)
            {
                GUI.Label(new Rect(0, row++ * TextHeight, width, TextHeight), string.Format("Round points: {0}", GameStatus.RoundPoints), TextStyle);
            }

            foreach (var bonus in GameStatus.RoundBonuses.Where(bonus => bonus.Value != 0))
            {
                GUI.Label(new Rect(0, row++ * TextHeight, width, TextHeight), string.Format("{0}: {1}", _guiTextsTranslations[bonus.Key], bonus.Value), TextStyle);
            }

            GUI.Label(new Rect(0, row++ * TextHeight, width, TextHeight), string.Format("Total Round points: {0}", GameStatus.RoundTotal), TextStyle);
            GUI.Label(new Rect(0, row++ * TextHeight, width, TextHeight), string.Format("Best round: {0}", GameStatus.BestRound), TextStyle);
            GUI.Label(new Rect(0, row++ * TextHeight, width, TextHeight), "-  -  -  -", TextStyle);

            var levelUpText = "";
            if (GameStatus.LevelUp)
            {
                levelUpText = "Level Up!";
            }
            if (GameStatus.PowerUpUnlock)
            {
                levelUpText += " New PowerUp Earned!";
            }
            if (GameStatus.MapUnlocked)
            {
                levelUpText += " New Map Unlocked!";
            }

            if (levelUpText != "")
            {
                GUI.Label(new Rect(0, row++ * TextHeight, width, TextHeight), levelUpText, ThirdTitle);
            }

            GUI.Label(new Rect(0, row++ * TextHeight, width, TextHeight), string.Format("Your level: {0}", GameStatus.Level), TextStyle);
            
            GUI.Label(new Rect(0, row * TextHeight, width, TextHeight), string.Format("Level progress: {0}% ({1} / {2})",
                Math.Floor(100.0 * GameStatus.Experience / GameStatus.NextLevel),
                GameStatus.Experience - GameStatus.LastLevel,
                GameStatus.NextLevel - GameStatus.LastLevel), TextStyle);
            GUI.EndGroup();

            if (GUI.Button(new Rect(_width - ButtonWidth - Padding, _height - ButtonHeight - Padding, ButtonWidth, ButtonHeight), "Next", PlayButton))
            {
                // Show power up unlock screen or map select
                GuiState = GameStatus.PowerUpUnlock ? GuiState.PowerUpUnlock : GuiState.MapSelect;
            }

            GUI.EndGroup();
        }


        private void PowerUpUnlockGui()
        {
            var halfWidth = _width / 2 - 2 * Padding;
            var middleHeight = _height - 2 * Padding - TitleHeight - ButtonHeight;

            GUI.BeginGroup(new Rect(Screen.width / 2 - _width / 2, Screen.height / 2 - _height / 2, _width, _height));
            GUI.Box(new Rect(0, 0, _width, _height), "", Box);

            DrawMainTitle();

            GUI.BeginGroup(new Rect(0, TitleHeight + Padding, _width, middleHeight));
            GUI.Label(new Rect(0, 0, _width, TitleHeight), "Unlock PowerUp or HardUp", SecondTitle);

            DrawPowerUps(halfWidth, middleHeight, UnlockPowerUp, ResolvePowerUpToggleStyleInverse);

            DrawNegativePowerUps(halfWidth, middleHeight, UnlockPowerUp, ResolvePowerUpToggleStyleInverse);
            GUI.EndGroup();

            if (GUI.Button(new Rect(_width - ButtonWidth - Padding, _height - ButtonHeight - Padding, ButtonWidth, ButtonHeight), "Next", PlayButton))
            {
                var unlockedPowerUp = _selectedPowerUp.FirstOrDefault(x => x.Value).Key;

                if (unlockedPowerUp != null)
                {
                    GameStatus.UnlockedPowerUps.Add(unlockedPowerUp);
                    GuiState = GuiState.MapSelect;
                }
            }

            GUI.EndGroup();
        }


        private void PowerUpSelectGui()
        {
            var halfWidth = _width / 2 - 2 * Padding;
            var middleHeight = _height - 2 * Padding - TitleHeight - ButtonHeight;

            GUI.BeginGroup(new Rect(Screen.width / 2 - _width / 2, Screen.height / 2 - _height / 2, _width, _height));
            GUI.Box(new Rect(0, 0, _width, _height), "", Box);

            DrawMainTitle();

            GUI.BeginGroup(new Rect(0, TitleHeight + Padding, _width, middleHeight));
            GUI.Label(new Rect(0, 0, _width, TitleHeight), "Select PowerUp or HardUp", SecondTitle);

            DrawPowerUps(halfWidth, middleHeight, SelectPowerUp, ResolvePowerUpToggleStyle );

            DrawNegativePowerUps(halfWidth, middleHeight, SelectPowerUp, ResolvePowerUpToggleStyle);

            GUI.EndGroup();

            // Bottom
            if (GUI.Button(new Rect(Padding, _height - ButtonHeight - Padding, ButtonWidth, ButtonHeight), "Back", PlayButton))
            {
                GuiState = GuiState.MapSelect;
            }

            if (GUI.Button(new Rect(_width - ButtonWidth - Padding, _height - ButtonHeight - Padding, ButtonWidth, ButtonHeight), "Play", PlayButton))
            {
                StartRound();
            }

            GUI.EndGroup();
        }

        private void DrawMainTitle()
        {
            GUI.Label(new Rect(0, 0, _width, TitleHeight + Padding), "Scotsman vs Vikings", MainTitle);
        }

        private void StartRound()
        {

            GameStatus.ActivePowerUp = null;
            GameStatus.ActiveNegativePowerUp = null;

            foreach (var powerUp in Helpers.EnumerateEnum<PowerUp>())
            {
                if (_selectedPowerUp[powerUp])
                    GameStatus.ActivePowerUp = powerUp;
            }
            foreach (var negativePowerUp in Helpers.EnumerateEnum<NegativePowerUp>())
            {
                if (_selectedPowerUp[negativePowerUp])
                    GameStatus.ActiveNegativePowerUp = negativePowerUp;
            }
            foreach (var level in _selectedMap)
            {
                if (level.Value)
                    GameStatus.Map = level.Key;
            }


            GameFlowControlSignal.Dispatch(GameFlowAction.Start);
        }

        private void DrawNegativePowerUps(int halfWidth, int middleHeight, Action<bool, Enum> selectPowerUp, Func<Enum, GUIStyle> resolvePowerUpToggleStyle)
        {
            // Controls
            GUI.BeginGroup(new Rect(halfWidth+ 2* Padding, TitleHeight, halfWidth, middleHeight - TitleHeight));
            AddPowerUpToggles(halfWidth, selectPowerUp, resolvePowerUpToggleStyle, Helpers.EnumerateEnumAsEnums<NegativePowerUp>(), "HardUps");
            GUI.EndGroup();
        }

        private void DrawPowerUps(int halfWidth, int middleHeight, Action<bool, Enum> selectPowerUp, Func<Enum, GUIStyle> resolvePowerUpToggleStyle)
        {
            // Controls
            GUI.BeginGroup(new Rect(Padding, TitleHeight, halfWidth, middleHeight - TitleHeight));
            AddPowerUpToggles(halfWidth, selectPowerUp, resolvePowerUpToggleStyle, Helpers.EnumerateEnumAsEnums<PowerUp>(), "PowerUps");
            GUI.EndGroup();
        }

        private void AddPowerUpToggles(float width,  Action<bool, Enum> selectAction, Func<Enum, GUIStyle> styleResolver, IEnumerable<Enum> powerUps, string text)
        {
            var row = 0;
            GUI.Label(new Rect(0, row++*TextHeight, width, TextHeight), text, ThirdTitle);

            foreach (var powerUp in powerUps)
            {
                selectAction(GUI.Toggle(new Rect(0, row++ * TextHeight, width, TextHeight), _selectedPowerUp[powerUp], _guiTextsTranslations[powerUp], styleResolver(powerUp)), powerUp);
            }
        }

        private GUIStyle ResolvePowerUpToggleStyle(Enum powerUp)
        {
            return GameStatus.UnlockedPowerUps.Contains(powerUp) ? PowerUpButton : DisabledPowerUpButton;
        }


        private GUIStyle ResolvePowerUpToggleStyleInverse(Enum powerUp)
        {
            return GameStatus.UnlockedPowerUps.Contains(powerUp) ? DisabledPowerUpButton  : PowerUpButton;
        }

        private void SelectPowerUp(bool toggle, Enum powerUp)
        {
            // Check if power up is unlocked
            if (!GameStatus.UnlockedPowerUps.Contains(powerUp)) return;

            if (toggle) ClearPowerUps();
            _selectedPowerUp[powerUp] = toggle;
        }

        private void ClearPowerUps()
        {
            foreach (var powerUp in Helpers.EnumerateEnum<PowerUp>())
            {
                _selectedPowerUp[powerUp] = false;
            }
            foreach (var negativePowerUp in Helpers.EnumerateEnum<NegativePowerUp>())
            {
                _selectedPowerUp[negativePowerUp] = false;
            }
        }

        public void DisplayGuiText(GuiText text)
        {
            _guiTexts.Add(new KeyValuePair<float, GuiText>(Time.time + ShowGuiTextTime, text));
        }

        public void Init()
        {
            GuiState = GuiState.MapSelect;

            _selectedPowerUp = new Dictionary<Enum, bool>();
            foreach (var powerUp in Helpers.EnumerateEnum<PowerUp>())
            {
                _selectedPowerUp.Add(powerUp, false);
            }
            foreach (var negativePowerUp in Helpers.EnumerateEnum<NegativePowerUp>())
            {
                _selectedPowerUp.Add(negativePowerUp, false);
            }

            _selectedMap = new Dictionary<string, bool>();
            foreach (var map in MapStorage.Maps)
            {
                _selectedMap.Add(map.Key, false);
            }

            _selectedMap[MapStorage.Maps.First().Key] = true;
        }
    }
}
