﻿using System.Timers;
using Game.Signals;
using strange.extensions.mediation.impl;

namespace Game.Views
{
    public class TimerView : View
    {
        private bool _tick;
        private Timer _timer;
        private bool _timerEnabled;

        [Inject]
        public GameActionSignal GameActionSignal { get; set; }

        [Inject]
        public GameParameters Parameters { get; set; }


        public void Update()
        {
            if (!_tick) return;

            GameActionSignal.Dispatch(GameAction.Pause);
            _tick = false;
        }

        public void StartTicker()
        {
            _timerEnabled = true;
            _timer = new Timer(Parameters.RealTimeDelay);
            _timer.Elapsed += OnTick;
            _timer.Enabled = true;
        }

        public void ResetTicker()
        {
            if (!_timerEnabled) return;
            _timer.Stop();
            _timer.Start();
        }

        public void StopTicker()
        {
            if (!_timerEnabled) return;
            _timerEnabled = false;
            _timer.Stop();
        }

        private void OnTick(object sender, ElapsedEventArgs e)
        {
            _tick = true;
        }
    }

}