﻿using System;
using System.Collections.Generic;
using Game.Map;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Game.Views
{
    public class MapView : View
    {
        [Inject]
        public ILevel Level { get; set; }

        public GameObject RoadPrefab;
        public GameObject GrassPrefab;
        public GameObject DarkPrefab;

        private List<GameObject> _tiles;
        private Dictionary<Position, GameObject> _visited;

        public GameObject Grass0;
        public GameObject Grass1;
        public GameObject Grass2;
        public GameObject Grass3;
        public GameObject Grass4;
        public GameObject Grass5;
        public GameObject Grass6;
        public GameObject Grass7;
        public GameObject Grass8;
        public GameObject Grass9;
        public GameObject Grass10;
        public GameObject Grass11;
        public GameObject Grass12;
        public GameObject Grass13;
        public GameObject Grass14;
        public GameObject Grass15;

        public void DrawTiles()
        {
            Init();

            foreach (var mapPoint in Level.Map)
            {
                _tiles.Add(InstantiateGameObject(mapPoint.Position, MapPointToGameObject(mapPoint)));
            }
        }

        public void DrawFogOfWar()
        {
            foreach (var mapPoint in Level.Map)
            {
                if (mapPoint.Visible)
                {
                    ViewHelpers.SetSpriteAlpha(_visited[mapPoint.Position], 0);
                }
                else if (mapPoint.Visited)
                {
                    ViewHelpers.SetSpriteAlpha(_visited[mapPoint.Position], 0.5f);
                }
                else
                {
                    ViewHelpers.SetSpriteAlpha(_visited[mapPoint.Position], 1);
                }
            }
        }

        private static GameObject InstantiateGameObject(Position position, GameObject prefab)
        {
            return (GameObject)Instantiate(
                prefab,
                position.GetVector3(),
                Quaternion.identity);
        }

        private void Init()
        {
            InitTiles();
            InitVisited();
        }

        private void InitVisited()
        {
            ViewHelpers.DestroyGameObjects(_visited);
            _visited = new Dictionary<Position, GameObject>();

            foreach (var mapPoint in Level.Map)
            {
                _visited.Add(mapPoint.Position, InstantiateGameObject(mapPoint.Position, DarkPrefab));
            }

        }

        private void InitTiles()
        {
            ViewHelpers.DestroyGameObjects(_tiles);
            _tiles = new List<GameObject>();
        }

        private GameObject MapPointToGameObject(MapPoint mapPoint)
        {
            switch (mapPoint.Type)
            {
                case MapPointType.Grass:
                    return SelectGrass(mapPoint);
                case MapPointType.Road:
                    return RoadPrefab;
            }

            throw new ArgumentException(string.Format("Couldn't resolve prefab for '{0}'", mapPoint.Position));
        }

        private GameObject SelectGrass(MapPoint mapPoint)
        {
            var tile = 0;
            
            if (IsThereRoadAt(mapPoint, Direction.Up))
            {
                tile += 1;
            }
            if (IsThereRoadAt(mapPoint, Direction.Right))
            {
                tile += 2;
            }
            if (IsThereRoadAt(mapPoint, Direction.Down))
            {
                tile += 4;
            }
            if (IsThereRoadAt(mapPoint, Direction.Left))
            {
                tile += 8;
            }

            return (GameObject) GetType().GetField("Grass" + tile).GetValue(this);
        }

        private bool IsThereRoadAt(MapPoint point, Direction direction)
        {
            var tile = Level.GetPointAt(point.Position, direction);
            return tile != null && tile.Type == MapPointType.Road;
        }
    }
}
