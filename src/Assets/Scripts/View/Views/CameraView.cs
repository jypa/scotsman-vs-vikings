﻿using strange.extensions.mediation.impl;
using UnityEngine;

namespace Game.Views
{
    public class CameraView : View
    {
        public float Speed = 5;

        private Vector3 _position;

        public void Move(Position position)
        {
            _position = GetNewCameraPosition(position); 
        }

        private static Vector3 GetNewCameraPosition(Position position)
        {
            var newPosition = position.GetVector3();
            newPosition.z = -1;
            return newPosition;
        }

        public void Update()
        {
            transform.position = Vector3.MoveTowards(transform.position, _position, Speed * Time.deltaTime);
        }

        public void Jump(Position position)
        {
            _position = GetNewCameraPosition(position);
            transform.position = GetNewCameraPosition(position);
        }
    }
}