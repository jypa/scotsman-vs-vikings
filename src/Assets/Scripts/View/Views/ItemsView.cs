﻿using System.Collections.Generic;
using Game.Map;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Game.Views
{
    public class ItemsView : View
    {
        [Inject]
        public ILevel Level { get; set; }

        public GameObject CoinPrefab;
        public GameObject WeaponPrefab;
        private Dictionary<Position, GameObject> _items;

        public void DestroyItem(GameItem gameItem)
        {
            if (!_items.ContainsKey(gameItem.MapPoint.Position)) return;

            Destroy(_items[gameItem.MapPoint.Position]);
            _items.Remove(gameItem.MapPoint.Position);
        }

        public void DrawItems()
        {
            Init();

            foreach (var mapPoint in Level.Map)
            {

                if (mapPoint.GenerationPointType == MapGenerationPointType.Coin)
                {
                    _items.Add(mapPoint.Position, InstantiateGameObject(mapPoint.Position, CoinPrefab));
                }

                if (mapPoint.GenerationPointType == MapGenerationPointType.Weapon)
                {
                    _items.Add(mapPoint.Position, InstantiateGameObject(mapPoint.Position, WeaponPrefab));
                }
            }
        }

        private void Init()
        {
            ViewHelpers.DestroyGameObjects(_items);
            _items = new Dictionary<Position, GameObject>();
        }

        private static GameObject InstantiateGameObject(Position position, GameObject prefab)
        {
            return (GameObject)Instantiate(
                prefab,
                position.GetVector3(),
                Quaternion.identity);
        }

    }
}