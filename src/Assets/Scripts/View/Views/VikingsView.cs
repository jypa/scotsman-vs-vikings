﻿using System.Collections.Generic;
using System.Linq;
using Game.Map;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Game.Views
{
    public class VikingsView : View
    {
        [Inject]
        public ILevel Level { get; set; }

        public GameObject VikingPrefab;
        public float Speed = 5;
        private Dictionary<int, VikingView> _vikings;

        public void Update()
        {
            foreach (var viking in _vikings.Select(vikingView => vikingView.Value))
            {
                viking.GameObject.transform.position = Vector2.MoveTowards(viking.GameObject.transform.position, viking.PositionVector2,
                    Speed*Time.deltaTime);
            }
        }

        public void AddVikings(List<VikingDto> vikings)
        {
            InitVikings();
            foreach (var viking in vikings)
            {
                AddViking(viking);
            }
        }

        public void AddViking(VikingDto viking)
        {
            var v = (GameObject)Instantiate(VikingPrefab, viking.Position.GetVector2(), Quaternion.identity);
            _vikings.Add(viking.Id, new VikingView
            {
                GameObject = v,
                Position = viking.Position
            });
        }

        private void InitVikings()
        {
            if (_vikings != null)
            {
                foreach (var viking in _vikings)
                {
                    Destroy(viking.Value.GameObject);
                }
            }

            _vikings = new Dictionary<int, VikingView>();
        }

        public void MoveViking(VikingDto vikingDto)
        {
            var vikingView = _vikings[vikingDto.Id];
            vikingView.Position = vikingDto.Position;
            ViewHelpers.SetOrderInLayer(vikingView.GameObject,  (100 - vikingView.Position.Y) * 10 + 3);
        }

        public void DestroyViking(VikingDto viking)
        {
            Destroy(_vikings[viking.Id].GameObject);
            _vikings.Remove(viking.Id);
        }

        public void UpdateVikingVisibility()
        {
            foreach (var viking in _vikings.Select(vikingView => vikingView.Value))
            {
                ViewHelpers.SetSpriteAlpha(viking.GameObject, Level.GetPoint(viking.Position).Visible ? 1 : 0);
            }
        }
    }

    internal class VikingView
    {
        private Position _position;
        public GameObject GameObject { get; set; }
        public Vector2 PositionVector2 { get; private set; }

        public Position Position
        {
            get { return _position; }
            set
            {
                _position = value;
                PositionVector2 = new Vector2(value.X, value.Y);
            }
        }
    }
}