﻿using strange.extensions.mediation.impl;
using UnityEngine;

namespace Game.Views
{
    public class ScotsmanView : View
    {
        public float Speed = 5;

        private Vector2 _position;
        private bool _startAttack;

        public Animator Animator { get; set; }

        public void Move(Position position)
        {
            _position = position.GetVector2();
            ViewHelpers.SetOrderInLayer(gameObject, (100- position.Y) * 10 + 2);
        }

        public void Update()
        {
            transform.position = Vector2.MoveTowards(transform.position, _position, Speed * Time.deltaTime);

            if (_position.y > transform.position.y)
            {
                Animator.SetInteger("scotsman", 3);
            }
            if (_position.y < transform.position.y)
            {
                Animator.SetInteger("scotsman", 2);
            }

            // Going right
            if (_position.x > transform.position.x)
            {
                Animator.SetInteger("scotsman", 1);
                transform.localScale = new Vector3(1, 1, 1);
            }
            // Going left
            if (_position.x < transform.position.x)
            {
                Animator.SetInteger("scotsman", 1);
                transform.localScale = new Vector3(-1, 1, 1);
            }


            if (_position.x == transform.position.x && _position.y == transform.position.y)
            {
                Animator.SetInteger("scotsman", 0);
            }

            if (_startAttack)
            {
                _startAttack = false;
                Animator.SetInteger("scotsman", 4);
            }

        }

        public void Jump(Position position)
        {
            var newPosition = position.GetVector2();
            _position = newPosition;
            transform.position = newPosition;
        }

        public void PlayAttackAnimation()
        {
            _startAttack = true;
        }
    }
}
